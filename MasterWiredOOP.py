from smbus import SMBus #importa la biblioteca SMBus (I2C)
import time #importa la biblioteca time

class Device:
    def __init__(self, address, bus): #Método Constructor: establece la dirección y el bus   
        self._bus = SMBus(bus) #inicializa la comunicacion I2C BUS
        self._address = address #Direccion a la que se conectara el maestro con el esclavo

    def getData(self): #Metodo getData (recibe la información de arduino y la muestra por pantalla)
        
        while True: #repetir en bucle

        	try: #codigo a ejecutar, pero podrian presentarse errores
        		#consigue los bytes obtenidos de la tx y los guarda en resultado	
        		result = self._bus.read_byte(self._address) & 0xff 
        		print('Temperatura [°C]: ') #imprime string por pantalla
        		print(result) #imprime por pantalla el valor de la temperatura

        		time.sleep(2) #breve delay de dos segundos

        	except KeyboardInterrupt: #el programa se detiene con CTRL+C (interrupcion teclado)               	
                    break

Arduino = Device(0x8, 1) #Direccion 8 y Bus 1
Arduino.getData() #El objeto Arduino utiliza el método "getData()"

