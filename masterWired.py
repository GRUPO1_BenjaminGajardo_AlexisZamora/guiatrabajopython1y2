from smbus import SMBus #importa la biblioteca SMBus (I2C)
import time #importa la biblioteca time

bus = SMBus(1) #inicializa la comunicacion I2C BUS
address = 0x8 #Direccion a la que se conectara el maestro con el esclavo
result = 0

while True: #repetir en bucle

    try: #codigo a ejecutar, pero podrian presentarse errores
        	#consigue los bytes obtenidos de la tx y los guarda en resultado	
        	result = bus.read_byte(address) & 0xff 
        	print('Temperatura [°C]: ') #imprime string por pantalla
        	print(result) #imprime por pantalla el valor de la temperatura

        	time.sleep(2) #breve delay de dos segundos

    except KeyboardInterrupt: #el programa se detiene con CTRL+C (interrupcion teclado)              	
            break



