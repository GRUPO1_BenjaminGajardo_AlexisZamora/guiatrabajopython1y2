import serial #se importa biblioteca serial de python

#objeto serial, se le indica donde esta conectado el arduino y la tasa de bits en baudios 9600
ser = serial.Serial("/dev/ttyACM0", 9600) 
ser.flushInput() #descarta lo contenido en el buffer serial
palabra = ''

while True:

        try: #codigo a ejecutar, pero podrian presentarse errores

                readByte = ser.read() #Lectura de datos que provienen del arduino, retorna un byte a la vez
                line = readByte.decode('utf-8') #se decodifica por utf-8, convierte bytes de arduino a cadena de texto (string)
              

                palabra += line #completa la cadena de texto "palabra" con los valores de line              	


                if line == ">": 
                        # solo se ingresara a este bloque de codigo si el string palabra termina con ">"
                        print("Temperatura [°C]: ") #se imprime por consola "Temperatura °C: "
                        print(palabra[1:5]) #se imprime por consola el string palabra desde el char 1 al 5 (valor temperatura)
                        print("Humedad [%]: ") #se imprime por consola "Humedad [%]: "
                        print(palabra[7:11]) #se imprime por consola el string palabra desde el char 7 al 11 (valor humedad)
                        palabra = "" #se reasigna el string palabra a ""

        except KeyboardInterrupt: #el programa se detiene con CTRL+C (interrupcion teclado)
                break


