Integrantes: Benjamin Gajardo y Alexis Zamora

6 programas:

Slave4.ino [Código Arduino. Comunicación serial raspberry-arduino]

SlaveWired3.ino [Código Arduino. Comunicación I2C raspberry-arduino]

master9.py [Código Python procedimental. Comunicación serial raspberry-arduino]

MasterOOP.py [Código Python orientado a objetos. Comunicación serial raspberry-arduino]

masterWired.py [Código Python procedimental. Comunicación I2C raspberry-arduino]

MasterWiredOOP.py [Código Python orientado a objetos. Comunicación I2C raspberry-arduino]