import serial
  
class Ardu: #Declaración de clase Ardu
    
    def __init__(self, port, bauds): #Método Constructor: establece el puerto y la tasa de baudios          
        
        self.port = port      #Argumento Self (uno mismo) para el puerto
        self.bauds = bauds    #Argumento Self (uno mismo) para los baudios
        

    def getData(self): #Metodo getData (recibe la información de arduino y la muestra por pantalla)

        #objeto serial (de biblioteca serial) se le pasa el objeto Arduino con sus atributos port y bauds
        ser = serial.Serial(Arduino.port, Arduino.bauds) 
        ser.flushInput() #descarta lo contenido en el buffer serial
        palabra = '' #se declara la variable   

        while True: #repetir en bucle
            #codigo a ejecutar, pero podrian presentarse errores
            try: 
                    #Lectura de datos que provienen del arduino, retorna un byte a la vez
                    readByte = ser.read()
                    #se decodifica por utf-8, convierte bytes de arduino a cadena de texto (string) 
                    line = readByte.decode('utf-8') 
                    
                    #completa la cadena de texto "palabra" con los valores de line      
                    palabra += line                 

                    #solo se ingresara a este bloque de codigo si el string palabra termina con ">"
                    if line == ">": 
                        
                        #se imprime por consola "Temperatura °C: "
                        print("Temperatura [°C]: ")
                        #se imprime por consola el string palabra desde el char 1 al 5 (valor temperatura) 
                        print(palabra[1:5])
                        #se imprime por consola "Humedad [%]: " 
                        print("Humedad [%]: ")
                        #se imprime por consola el string palabra desde el char 7 al 11 (valor humedad) 
                        print(palabra[7:11]) 
                        palabra = "" #se reasigna el string palabra a "" 

            except KeyboardInterrupt: #el programa se detiene con CTRL+C (interrupcion teclado)
                    break

Arduino = Ardu("/dev/ttyACM0", 9600) #Creación del objeto Arduino
Arduino.getData() #El objeto Arduino utiliza el método "getData()"


