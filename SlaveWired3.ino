#include "Adafruit_Sensor.h" // Se importa la biblioteca Adafruit_Sensor (Sensor)
#include "DHT.h" // Se importa la biblioteca DHT.h (Sensor)
#include "Wire.h" // Se importa la biblioteca Wire (Comunicacion I2C)

#define DHTPIN 2       // El pin de informacion es el numero 2
#define DHTTYPE DHT11   // Sensor DHT 11

const byte ADDRESS = 0x8; // Se asigna la variable para la direccion 0x8

DHT dht(DHTPIN, DHTTYPE); // indica pin de lectura y modelo de sensor 

byte temp = 0; // declara la variable temp de tipo byte

void setup() {
  dht.begin(); // inicializa el sensor dht

  Wire.begin(ADDRESS); // Es la misma direccion que para la raspberry (maestro)
  Wire.onReceive(request); // Funcion de recepcion de datos
  Wire.onRequest(response); // Funcion de envio de datos
}

void response() {
  Wire.write(temp); // Envia el valor almacenado en temp (valor temperatura [°C])
}

void request(int count) {
  while (Wire.available() > 0) { // se ejecutara mientras hayan bytes por leer
    byte b = Wire.read(); // lectura
  }
}

void loop() {
  long next = millis() + 1000; // se repite cada 1 segundo
  while (millis() < next) {}

  temp = (byte) dht.readTemperature(); // lectura de temperatura
}
