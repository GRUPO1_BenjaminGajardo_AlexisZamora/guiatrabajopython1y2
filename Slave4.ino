#include <Adafruit_Sensor.h> // incluye libreria Adafruit Sensor
#include <DHT.h> // sensor DHT
#define DHTPIN 2 // indica que la lectura se hace en el pin 2
#define DHTTYPE DHT11 // indica que se utilizara DHT11

DHT dht(DHTPIN, DHTTYPE); // indica el pin de lectura y el modelo del sensor "dht(2, DHT11)"
long anterior = 0; // inicialización de variable

void setup() {
  Serial.begin(9600); // tasa de transferencia de datos en 9600 baudios
  dht.begin(); // inicializa el sensor DHT
}

void loop() {
  if(millis() - anterior >= 1000) // repetir cada 1 segundo (reemplaza a delay)
    {
      float t = dht.readTemperature(); // Lectura de temperatura °C
      float h = dht.readHumidity(); // Lectura de humedad

      Serial.print("<"); // imprime caracter "<" por monitor serial
      Serial.print(t);  // imprime temperatura por monitor serial
      Serial.print("-"); // imprime caracter "-" por monitor serial
      Serial.print(h); // imprime humedad por monitor serial
      Serial.print(">"); // imprime caracter "<" por monitor serial

      anterior = millis(); // asigna a anterior el valor de millis()
    }
}
